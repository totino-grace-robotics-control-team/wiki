# Wiki - what I know is

This wiki will attempt to serve as to second "go to location" for any of your programming/development questions. I want this to be the second location because FRC has already compiled their own amazing wiki and most answers can be found there. Don't forget to look on ChiefDelfi either!

 - [FRC docs](https://docs.wpilib.org/en/stable/) in depth resource containing documentation for the all control systems of the robot
 - [Chief Delphi](https://www.chiefdelphi.com/) forum with high level topics about all parts of robots

## Pizza π's specific resources

resources for anything else not found on hte frc docs or chiefDelphi

### laptop development tools

 - [Installing our laptop development tools]()

### learning java

below are some specific resources for learning java adapted from "The Coding Train". I only have these as Edpuzzles so I can track people progress through them

 - [0.1: Who are you? Who am I? - Processing Tutorial](https://edpuzzle.com/assignments/5fc45b1e5b3ca4409332bfe2/watch)
 - [0.2: How and why should you learn Programming? - Processing Tutorial](https://edpuzzle.com/assignments/5fc462fa21a78440a8789ebe/watch)
 - [0.3: What is programming? - Processing Tutorial](https://edpuzzle.com/assignments/5fc4687a814f0a40d0954bc9/watch)
 - [0.4: What are some programming languages? - Processing Tutorial](https://edpuzzle.com/assignments/5fc46fe6814f0a40d095ccc9/watch)
 - [0.5: Processing Examples - Processing Tutorial](https://edpuzzle.com/assignments/5fc531d0c2735140a858268a/watch)
 - [0.6: Overview of Topics - Processing Tutorial](https://edpuzzle.com/assignments/5fc53504b7018e40819696ab/watch)
 - [1.1: Drawing with Pixels - Processing Tutorial](https://edpuzzle.com/assignments/5fc53a543281fe409cd74307/watch)
 - [2.1: How to use Processing - Processing Tutorial](https://edpuzzle.com/assignments/5fc53e66a2e42d40b3988145/watch)
 - [2.2: RGB Color - Processing Tutorial](https://edpuzzle.com/assignments/5fc5533d973bc240da6c99c5/watch)
 - [3.1: Flow (setup and draw) - Processing Tutorial](https://edpuzzle.com/assignments/5fc556e0cba57740e0bcaa71/watch)
 - [3.2: Built-in Variables (mouseX, mouseY) - Processing Tutorial](https://edpuzzle.com/assignments/5fc559d904a1b740f38d547d/watch)
 - [3.3: Events (mousePressed, keyPressed) - Processing Tutorial](https://edpuzzle.com/assignments/5fc55c81e1f8b340a593d39e/watch)
 - [4.1: Variables - Processing Tutorial](https://edpuzzle.com/assignments/5fc55d1496c39c40b05611b7/watch)
 - [4.2: Incrementing a Variable - Processing Tutorial](https://edpuzzle.com/assignments/5fc56082e1f8b340a593eb24/watch)
 - [4.3: Using random() - Processing Tutorial](https://edpuzzle.com/assignments/5fc577ddf7abdf40900fe7ca/watch)
 - [4.4: Using println() - Processing Tutorial](https://edpuzzle.com/assignments/5fc579178c02df40e71b73bb/watch)
 - [5.1: Boolean Expressions - Processing Tutorial](https://edpuzzle.com/assignments/5fc579c286dfcb40b351cac2/watch)
 - [5.2: If, Else If, Else - Processing Tutorial](https://edpuzzle.com/assignments/5fc57b34662f0140a310925d/watch)
 - [5.3: Logical Operators: AND, OR - Processing Tutorial](https://edpuzzle.com/assignments/5fc57ed4054ed04096595437/watch)
 - [5.4: Boolean Variables - Processing Tutorial](https://edpuzzle.com/assignments/5fc57f8f7c801740dd48a1b5/watch)
 - [5.5: The Bouncing Ball - Processing Tutorial](https://edpuzzle.com/assignments/5fc58018f9677a40aa17ee51/watch)
 - [6.1: While Loop - Processing Tutorial](https://edpuzzle.com/assignments/5fc582dcc13117409e72f5b6/watch)
 - [6.2: Two Loops / Grid Exercise - Processing Tutorial](https://edpuzzle.com/assignments/5fc58449fa666a40cf384dca/watch)
 - [6.3: For Loop - Processing Tutorial](https://edpuzzle.com/assignments/5fc5877498814140af932e7b/watch)
 - [6.4: Variable Scope - Processing Tutorial](https://edpuzzle.com/assignments/5fc58811f6e0134072ae6019/watch)
 - [6.5: Loop vs. Draw - Processing Tutorial](https://edpuzzle.com/assignments/5fc58a3633028740dc99f434/watch)
 - [6.6: Nested Loops - Processing Tutorial](https://edpuzzle.com/assignments/5fc58b054570ff40a9577656/watch)
 - [7.1: Introduction to Functions and Objects - Processing Tutorial](https://edpuzzle.com/assignments/5fc58bfa56549d40acfaca15/watch)
 - [7.2: Functions Basics - Processing Tutorial](https://edpuzzle.com/assignments/5fc58c990bcd3d40c463c78d/watch)
 - [7.3: Modularity with Functions - Processing Tutorial](https://edpuzzle.com/assignments/5fc58db98ff6d840c3363b68/watch)
 - [7.4: Reusability with Functions - Processing Tutorial](https://edpuzzle.com/assignments/5fc58ea938a5ac40c19c98b1/watch)
 - [8.1: What is Object-Oriented Programming (OOP)? - Processing Tutorial](https://edpuzzle.com/assignments/5fc58fd238a5ac40c19ca062/watch)
 - [8.2: Defining a Class Part I - Processing Tutorial](https://edpuzzle.com/assignments/5fc590f6f671bf4093aa11b9/watch)
 - [8.3: Defining a Class Part II - Processing Tutorial](https://edpuzzle.com/assignments/5fc59165097ef8409ef9ce86/watch)
 - [8.4: Constructor Arguments - Processing Tutorial](https://edpuzzle.com/assignments/5fc5919da6a26840f5f49b94/watch)
 - [8.5: More on Objects - Processing Tutorial](https://edpuzzle.com/assignments/5fc5925eb7bcb440dd9abe8d/watch)
 - [8.6: Pass by Value vs. Pass by Reference - Processing Tutorial](https://edpuzzle.com/assignments/5fc592ce2e61ae40cb7f2aa5/watch)
 - [9.1: What is an Array? - Processing Tutorial](https://edpuzzle.com/assignments/5fc5938785fc9a40e4deb586/watch)
 - [9.2: Declare, Initialize, and Use an Array - Processing Tutorial](https://edpuzzle.com/assignments/5fc5942c74105740a24323f8/watch)
 - [9.3: Arrays of Objects - Processing Tutorial](https://edpuzzle.com/assignments/5fc5953a85fc9a40e4debb0b/watch)
 - [9.4: Arrays and Loops - Processing Tutorial](https://edpuzzle.com/assignments/5fc59565054ed0409659bfee/watch)
 - [9.5: Arrays of Flexible Size - Processing Tutorial](https://edpuzzle.com/assignments/5fc59594f43f1140944fe904/watch)

### java challenges

 - [bunch of challenges you can do in java - Processing](https://www.youtube.com/watch?v=17WoOqgXsRM&list=PLRqwX-V7Uu6ZiZxtDDRCi6uhfTH4FilpH)

### learn git

once again from "The Coding Train"

 - [1.1: Introduction - Git and GitHub for Poets](https://www.youtube.com/watch?v=BCQHnlnPusY)
 - [1.2: Branches - Git and GitHub for Poets](https://www.youtube.com/watch?v=oPpnCh7InLY)
 - [1.3: Forks and Pull Requests - Git and GitHub for Poets](https://www.youtube.com/watch?v=_NrSWLQsDL4)
 - [1.4: GitHub Issues - Git and GitHub for Poets](https://www.youtube.com/watch?v=WMykv2ZMyEQ)
 - [1.5: Intro to the Command Line - Git and GitHub for Poets](https://www.youtube.com/watch?v=oK8EvVeVltE)
 - [1.6: Cloning Repo and Push/Pull - Git and GitHub for Poets](https://www.youtube.com/watch?v=yXT1ElMEkW8)
 - [1.7: git init and git add - Git and GitHub for Poets](https://www.youtube.com/watch?v=9p2d-CuVlgc)
 - [1.8: GitHub Pages - Git and GitHub for Poets](https://www.youtube.com/watch?v=bFVtrlyH-kc)
 - [1.9: Resolving Merge Conflicts - Git and GitHub for Poets](https://www.youtube.com/watch?v=JtIX3HJKwfo)
 - [1.10: Git Remotes - Git and GitHub for Poets](https://www.youtube.com/watch?v=lR_hYwCAaH4)

### learn bash, zsh, command line, and PATH

It helps if you watch these videos in order. Once again from "The Coding Train"

 - [Introduction to My Workflow](https://www.youtube.com/watch?v=gJa6wri8YNQ)
 - [Workflow: Visual Studio Code](https://www.youtube.com/watch?v=yJw0SyKO9IU)
 - [Workflow: Shell](https://www.youtube.com/watch?v=FnkkzgYuXUM)
 - [Workflow: Git](https://www.youtube.com/watch?v=_sLgRBrZh6o)
 - [Workflow: Node](https://www.youtube.com/watch?v=FjWbUK2HdCo)
 - [Workflow: Python and Virtualenv](https://www.youtube.com/watch?v=nnhjvHYRsmM)

### random videos to keep you up at night (they are interesting)

These videos are some of my favorite videos that make me think

 - [Object-Oriented Programming is Bad](https://www.youtube.com/watch?v=QM1iUe6IofM)
 - [Object-Oriented Programming is Good*](https://www.youtube.com/watch?v=0iyB0_qPvWk)
 - [Functional architecture - The pits of success - Mark Seemann](https://www.youtube.com/watch?v=US8QG9I1XW0)
 - [Why Isn't Functional Programming the Norm? – Richard Feldman](https://www.youtube.com/watch?v=QyJZzq0v7Z4)
 - [Domain Driven Design: The Good Parts - Jimmy Bogard](https://www.youtube.com/watch?v=U6CeaA-Phqo)
 - [Avoiding Microservice Megadisasters - Jimmy Bogard](https://www.youtube.com/watch?v=gfh-VCTwMw8)
 - [Refactoring to Immutability - Kevlin Henney](https://www.youtube.com/watch?v=APUCMSPiNh4)

other talks from NDC Conferences:
 - [Pushing C# to the limit - Joe Albahari](https://www.youtube.com/watch?v=mLX1sYVf-Xg)
 - [Clean Coders Hate What Happens to Your Code When You Use These Enterprise Programming Tricks](https://www.youtube.com/watch?v=FyCYva9DhsI)

also check out some of the talks by Deviant Ollam and Babak Javadi at Def Con. Some of the top Def Con talks in my opinion include:
 - [DEFCON 17: That Awesome Time I Was Sued For Two Billion Dollars](https://www.youtube.com/watch?v=KSWqx8goqSY)
 - [Babak Javadi - Basics of Hacking Physical Access Control Systems - DEF CON 27 Wireless Village](https://www.youtube.com/watch?v=LS5OQHUJaJE)
 - [The Search for the Perfect Door - Deviant Ollam](https://www.youtube.com/watch?v=4YYvBLAF4T8&t=260s)
 - [DEF CON 22 - Deviant Ollam & Howard Payne - Elevator Hacking - From the Pit to the Penthouse](https://www.youtube.com/watch?v=oHf1vD5_b5I)
 - [DEFCON 19: The Art of Trolling](https://www.youtube.com/watch?v=AHqGV5WjS4w)
 - [Hack All The Things](https://www.youtube.com/watch?v=h5PRvBpLuJs)
 - [Bill Swearingen - DEF CON 27 Conference](https://www.youtube.com/watch?v=vQtLms02PFM)
 - [Roger Dingledine - The Tor Censorship Arms Race The Next Chapter - DEF CON 27 Conference](https://www.youtube.com/watch?v=ZB8ODpw_om8)
 - [I'll Let Myself In: Tactics of Physical Pen Testers](https://www.youtube.com/watch?v=rnmcRTnTNC8)
 - [Elevator Hacking: From the Pit to the Penthouse](https://www.youtube.com/watch?v=ZUvGfuLlZus)

### Other YouTube channels I like (You might like them too)

 - [Linus Tech Tips](https://www.youtube.com/channel/UCXuqSBlHAE6Xw-yeJA0Tunw)
 - [The Coding Train](https://www.youtube.com/channel/UCvjgXvBlbQiydffZU7m1_aw)
 - [LiveOverflow](https://www.youtube.com/channel/UClcE-kVhqyiHCcjYwcpfj9w)
 - [TechLinked](https://www.youtube.com/channel/UCeeFfhMcJa1kjtfZAGskOCA)
 - [javidx9](https://www.youtube.com/channel/UC-yuWVUplUJZvieEligKBkA)
 - [Ben Eater](https://www.youtube.com/channel/UCS0N5baNlQWJCUrhCEo8WlA)
 - [Tom Scott](https://www.youtube.com/channel/UCBa659QWEk1AI4Tg--mrJ2A)
 - [William Osman](https://www.youtube.com/channel/UCfMJ2MchTSW2kWaT0kK94Yw)
 - [Code Bullet](https://www.youtube.com/channel/UC0e3QhIYukixgh5VVpKHH9Q)
 - [Lemmino](https://www.youtube.com/channel/UCRcgy6GzDeccI7dkbbBna3Q)
 - [James Bruton](https://www.youtube.com/channel/UCUbDcUPed50Y_7KmfCXKohA)
 - [Creel](https://www.youtube.com/channel/UCq7dxy_qYNEBcHqQVCbc20w)
 - [Micheal Reeves](https://www.youtube.com/channel/UCtHaxi4GTYDpJgMSGy7AeSw)
 - [Devon Crawford](https://www.youtube.com/channel/UCDrekHmOnkptxq3gUU0IyfA)
